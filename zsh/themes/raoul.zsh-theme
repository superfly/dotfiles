# -*- sh -*- vim:set ft=sh ai et sw=4 sts=4:
# It might be bash like, but I can't have my co-workers knowing I use zsh
PROMPT='%{$fg_bold[green]%}%n@%m%{$reset_color%}:%{$fg_bold[blue]%}%~%{$reset_color%}$(git_prompt_info)$(hg_prompt_info)%(!.#.$) '

PROMPT_PREFIX=" %{$fg[magenta]%}"
PROMPT_SUFFIX="%{$reset_color%} "
PROMPT_CLEAN=""
PROMPT_DIRTY=" %{$fg[yellow]%}✗%{$reset_color%}"

# git
ZSH_THEME_GIT_PROMPT_PREFIX=$PROMPT_PREFIX
ZSH_THEME_GIT_PROMPT_SUFFIX=$PROMPT_SUFFIX
ZSH_THEME_GIT_PROMPT_DIRTY=$PROMPT_DIRTY
ZSH_THEME_GIT_PROMPT_CLEAN=$PROMPT_CLEAN

# hg
ZSH_THEME_HG_PROMPT_PREFIX=$PROMPT_PREFIX
ZSH_THEME_HG_PROMPT_SUFFIX=$PROMPT_SUFFIX
ZSH_THEME_HG_PROMPT_DIRTY=$PROMPT_DIRTY
ZSH_THEME_HG_PROMPT_CLEAN=$PROMPT_CLEAN

# bzr
ZSH_THEME_BZR_PROMPT_PREFIX=$PROMPT_PREFIX
ZSH_THEME_BZR_PROMPT_SUFFIX=$PROMPT_SUFFIX
ZSH_THEME_BZR_PROMPT_DIRTY=$PROMPT_DIRTY
ZSH_THEME_BZR_PROMPT_CLEAN=$PROMPT_CLEAN

function bzr_prompt_info() {
	BZR_CB=`bzr nick 2> /dev/null | grep -v "ERROR" | cut -d ":" -f2 | awk -F / '{print $1}'`
	if [ -n "$BZR_CB" ]; then
		BZR_DIRTY=""
		if [[ -n `bzr status` ]]; then
            BZR_DIRTY=$ZSH_THEME_BZR_PROMPT_DIRTY
        else
            BZR_DIRTY=$ZSH_THEME_BZR_PROMPT_CLEAN
        fi
		echo "$ZSH_THEME_BZR_PROMPT_PREFIX$BZR_CB$BZR_DIRTY$ZSH_THEME_BZR_PROMPT_SUFFIX"
	fi
}
