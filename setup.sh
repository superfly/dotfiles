#!/bin/bash
# This script sets up the configuration from this repository

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
APT=`command -v apt`
DNF=`command -v dnf`

function check_or_install() {
    CMD=`command -v $1`
    PKG=$2
    if [[ -z "$CMD" ]]; then
        if [[ -n "$APT" ]]; then
            sudo apt install -y $PKG
        elif [[ -n "$DNF" ]]; then
            sudo dnf install -y $PKG
        fi
    fi

}

function setup_neovim() {
    # Set up Neovim
    if [[ -d "$HOME/.config/nvim" ]]; then
        return
    fi
    echo "Setting up neovim..."
    check_or_install nvim "neovim python3-neovim"
    cd $HOME/.config
    ln -s $DIR/nvim
    curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
           https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
}

function setup_git() {
    # Set up Git
    echo "Setting up git..."
    check_or_install git git
    git config --global alias.co checkout
    git config --global alias.ci "commit -a"
    git config --global alias.st status
    git config --global alias.br "checkout -b"
    git config --global alias.rv "reset --hard HEAD"
    git config --global alias.pum "pull upstream master"
    git config --global alias.pom "push origin master"
    git config --global alias.rbm "rebase master"
    git config --global alias.last "log -1 HEAD"
    git config --global alias.unstage "reset HEAD --"
    git config --global alias.uncommit "reset --soft HEAD^"
    git config --global alias.changelog "!_() { t=\$(git describe --abbrev=0 --tags); git log \${t}..HEAD --no-merges --pretty=format:'* %s'; }; _"
    git config --global pull.rebase false
}

function setup_zsh() {
    # Set up ZSH
    if [[ -d "$HOME/.oh-my-zsh" ]]; then
        return
    fi
    echo "Setting up zsh..."
    check_or_install zsh zsh
    sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
    cd $HOME/.oh-my-zsh/custom/themes
    ln -s $DIR/zsh/themes/raoul.zsh-theme raoul.zsh-theme
    sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="raoul"/' $HOME/.zshrc
}

function setup_topydo() {
    # Set up topydo
    if [[ -d "$HOME/.topydo" ]]; then
        return
    fi
    echo "Set up topydo (as 'todo')..."
    check_or_install virtualenv "virtualenv python3-virtualenv"
    cd $HOME/.config
    ln -s $DIR/topydo
    virtualenv -p python3 $HOME/.topydo
    $HOME/.topydo/bin/pip install topydo
    $HOME/.topydo/bin/pip install 'topydo[prompt]'
    $HOME/.topydo/bin/pip install 'topydo[columns]'
    cd $HOME/bin
    ln -s $HOME/.topydo/bin/topydo
    ln -s $HOME/.topydo/bin/topydo todo
}

# Set up a generic bin directory for scripts and tools
mkdir -p $HOME/bin
mkdir -p $HOME/.config
setup_neovim
setup_git
setup_zsh
setup_topydo
