set encoding=utf-8
scriptencoding utf-8
filetype plugin on

call plug#begin()
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'Shougo/vimproc.vim', { 'do' : 'make' }
Plug 'Valloric/python-indent'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'hynek/vim-python-pep8-indent'
Plug 'idanarye/vim-vebugger'
Plug 'itchyny/lightline.vim'
Plug 'janko-m/vim-test'
Plug 'leafgarland/typescript-vim'
Plug 'mhinz/vim-startify'
Plug 'mileszs/ack.vim'
Plug 'neomake/neomake'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'sjl/clam.vim'
Plug 'tpope/vim-vividchalk'
Plug 'zchee/deoplete-jedi'
" Plug 'mhartington/nvim-typescript', { 'do': ':UpdateRemotePlugins' }
" Plug 'HerringtonDarkholme/yats.vim'
Plug 'chiedo/vim-sort-blocks-by'
Plug 'kien/rainbow_parentheses.vim'
Plug 'rainglow/vim'
Plug 'yuezk/vim-js'
Plug 'maxmellon/vim-jsx-pretty'
Plug 'freitass/todo.txt-vim'
call plug#end()

set guifont=Hack:h10
set clipboard=unnamedplus
set noshowmode
set colorcolumn=100
set tw=0
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.pyo,build/*,*egg-info*,dist/*,node_modules/*,prime/*,stage/*
set number
set autoindent
set cindent
set smartindent
set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab
set termguicolors
colorscheme vividchalk

" Leader
let mapleader = ","
map <Leader><space> :noh<return>

" Lightline
let g:lightline = {
    \ 'colorscheme': 'jellybeans',
    \ 'component': {
    \   'filename': '%f',
    \   'readonly': '%{&filetype=="help"?"":&readonly?"":""}',
    \   'modified': '%{&filetype=="help"?"":&modified?"*":&modifiable?"":"-"}'
    \ },
    \ 'component_visible_condition': {
    \   'readonly': '(&filetype!="help"&& &readonly)',
    \   'modified': '(&filetype!="help"&&(&modified||!&modifiable))'
    \ },
    \ 'separator': { 'left': '', 'right': '' },
    \ 'subseparator': { 'left': '', 'right': '' }
    \ }

" CtrlP
" let g:ctrlp_cmd = 'CtrlPMixed'
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn|bzr|cvs)|node_modules$'

" Startify
let g:startify_change_to_vcs_root = 1
let g:startify_change_to_dir = 0
let g:startify_bookmarks = [ {'v': '~/.config/nvim/init.vim'}, {'z': '~/.zshrc'}, {'t': '~/.tmux.conf'} ]
let g:startify_list_order = [
    \ ['   Project Files'],
    \ 'dir',
    \ ['   Recent Files'],
    \ 'files',
    \ ['   Bookmarks'],
    \ 'bookmarks',
    \ ['   Other'],
    \ ]

" Neomake
let g:neomake_python_enabled_makers = ['flake8']
autocmd! BufWritePost * Neomake

" Deoplete
let g:deoplete#enable_at_startup = 1
let g:deoplete#sources#jedi#python_path = '/usr/bin/python3'

" Ack.vim
if executable('ag')
    let g:ackprg = 'ag --vimgrep --smart-case'
endif

" Vebugger
let g:vebugger_leader = '<Leader>d'

" TypeScript
let g:typescript_indent_disable = 1

" Black
let g:black_quiet = 1
let g:black_linelength = 100
" autocmd BufWritePost *.py execute ':Black'

" Specific file type options
autocmd FileType html setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType htmldjango setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType css setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType json setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType yaml setlocal shiftwidth=2 tabstop=2 softtabstop=2

" Set certain filetypes
autocmd BufRead,BufWritePost,BufNewFile Dockerfile.* setfiletype dockerfile
